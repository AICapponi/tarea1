package sample;

import java.util.ArrayList;

/**
 * Clase del cliente, del cual se crean instancias del mismo
 */
public class Cliente {
    private String nombre;
    private long identificacion;
    private String fechaNacimiento;
    private int edad;
    private String direccion;

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Clientes{" +
                "nombre='" + nombre + '\'' +
                ", identificacion=" + identificacion +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", direccion='" + direccion + '\'' +
                '}';
    }

    /**
     * Getters y Setters respectivos
     * @return Los get de cada atributo
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(long identificacion) {
        this.identificacion = identificacion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo constructor por defecto
     */
    public Cliente() {

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param nombre Nombre del cliente
     * @param identificacion Identificacion del cliente
     * @param fechaNacimiento Fecha de nacimiento del cliente
     * @param edad Edad del cliente
     * @param direccion Direccion del cliente
     */
    public Cliente(String nombre, long identificacion, String fechaNacimiento, int edad, String direccion) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.direccion = direccion;
    }
}
