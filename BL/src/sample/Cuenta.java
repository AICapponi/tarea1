package sample;

import javafx.scene.control.TextField;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase padre que le hereda a sus hijos (Corriente, CuentaAhorro, CuentaAhorroProg) sus atributos.
 */
public abstract class Cuenta {
    private int numeroCuenta;
    private double saldo;
    private LocalDate fechaApertura;
    private Cliente propietario;

    /**
     * Constructor por defecto de la clase, en caso de que no se pasen parametros
     */
    public Cuenta(){

    }

    /**
     * Constructor para asignarle los valores a las variables
     * @param numeroCuenta Numero de cuenta del cliente
     * @param saldo Saldo en la cuenta del cliente
     * @param fechaApertura Fecha de apertura de la cuenta del cliente
     * @param propietario Especifica el cliente propietario de esta cuenta
     */
    public Cuenta(int numeroCuenta, double saldo, LocalDate fechaApertura, Cliente propietario) {
        this.numeroCuenta = numeroCuenta;
        this.saldo = saldo;
        this.fechaApertura = fechaApertura;
        this.propietario = propietario;
    }

    /**
     * Getters y Setters de los atributos de la clase
     * @return Los valores de los atributos
     */
    public int getNumeroCuenta() { return numeroCuenta; }

    public void setNumeroCuenta(int numeroCuenta) { this.numeroCuenta = numeroCuenta; }

    public double getSaldo() { return saldo; }

    public void setSaldo(double saldo) { this.saldo = saldo; }

    public LocalDate getFechaApertura() { return fechaApertura; }

    public void setFechaApertura(LocalDate fechaApertura) { this.fechaApertura = fechaApertura; }

    public Cliente getPropietario() { return propietario; }

    public void setPropietario(Cliente propietario) { this.propietario = propietario; }

    /**
     * Metodo toString para devolver en un String los valores de los atributos en sus instancias
     * @return Valores de los atributos en sus instancias
     */
    @Override
    public String toString() {
        return "Cuenta{" +
                "numeroCuenta=" + numeroCuenta +
                ", saldo=" + saldo +
                ", fechaApertura='" + fechaApertura + '\'' +
                ", propietario=" + propietario +
                '}';
    }
}
