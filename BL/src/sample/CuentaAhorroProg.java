package sample;

import java.time.LocalDate;

/**
 * Clase hija de Cuenta, para el manejo de las instancias de cuentas de ahorro programado
 */
public class CuentaAhorroProg extends Cuenta{

    private Corriente cuentaVinculada;
    private double montoADebitar;
    static private final double tasaInteres = 0.05;
    private LocalDate ultimoDeposito = LocalDate.now();

    /**
     * Metodo constructor por defecto de la clase
     */
    public CuentaAhorroProg(){

    }

    /**
     * Metodo constructor para asignarle valores a las instancias de la misma clase a sus atributos
     * @param numeroCuenta Numero de cuenta de la cuenta abierta
     * @param saldo Saldo quetendra la cuenta al abrirse
     * @param fechaApertura Fecha de creacion de la cuenta
     * @param propietario Propietario Cliente de la cuenta
     * @param cuentaVinculada Especifica a que cuenta corriente esta vinculada
     * @param montoADebitar El monto que automaticamente jalara de la cuenta corriente a la que esta vinculada una vez al mes
     */
    public CuentaAhorroProg(int numeroCuenta, double saldo, LocalDate fechaApertura, Cliente propietario, Corriente cuentaVinculada, double montoADebitar) {
        super(numeroCuenta, saldo, fechaApertura, propietario);

        this.cuentaVinculada = cuentaVinculada;
        this.montoADebitar = montoADebitar;
    }

    /**
     * Getters y setters de los atributos, para llamarlos, definirle valores y usar sus valores
     * @return Los valores de lso atributos
     */
    public Corriente getCuentaVinculada() { return cuentaVinculada; }

    public void setCuentaVinculada(Corriente cuentaVinculada) { this.cuentaVinculada = cuentaVinculada; }

    public double getMontoADebitar() { return montoADebitar; }

    public void setMontoADebitar(double montoADebitar) { this.montoADebitar = montoADebitar; }

    public static double getTasaInteres() { return tasaInteres; }

    public LocalDate getUltimoDeposito() { return ultimoDeposito; }

    public void setUltimoDeposito(LocalDate ultimoDeposito) { this.ultimoDeposito = ultimoDeposito; }

    /**
     * Metodo toString para devolver una cadena de String con los valores de los atributos de la instancia de la clase
     * @return valores de los atributos de la instancia de la clase
     */
    @Override
    public String toString() {
        return "Cuenta{" +
                "numeroCuenta=" + getNumeroCuenta() +
                ", saldo=" + getSaldo() +
                ", fechaApertura='" + getFechaApertura() + '\'' +
                ", propietario=" + getPropietario() +
                ", cuentaVinculada" + getCuentaVinculada() +
                ", montoADebitar" + getMontoADebitar() +
                '}';
    }
}
