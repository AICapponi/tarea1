package sample;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;

public class BL {
    private static ArrayList<Corriente> cuentas = new ArrayList<>();
    private static ArrayList<CuentaAhorro> cuentasAhorro = new ArrayList<>();
    private static ArrayList<CuentaAhorroProg> cuentasAhorroP = new ArrayList<>();
    private static ArrayList<Cliente> clientes = new ArrayList<>();

    public static String registrarCliente (String nombre, long identificacion, String fechaNacimiento, int edad, String direccion){
        Cliente tmpCliente = new Cliente(nombre, identificacion, fechaNacimiento, edad, direccion);
        if (existeCliente(identificacion)) {
            return "Ya se ha registrado un cliente con ese numero de identificacion, por favor intente nuevamente";
        }
        clientes.add(tmpCliente); // Agrego un cliente al array list de clientes
        return "Cliente registrado con exito";
    }

    public static ArrayList<Cliente> listarClientes(){
        return clientes;
    }

    public static boolean existeCliente (long identificacion) {
        for (int i = 0; i < clientes.size(); i++){
            Cliente c = clientes.get(i);
            if (c.getIdentificacion() == identificacion) {
                return true;
            }
        }
        return false;
    }

    public static String registrarCuentasCorrientes (int numeroCuenta, Double saldo, LocalDate fechaApertura, long idCliente) {
        if (existeCuentaCorriente(numeroCuenta)) {
            return "Ya existe una cuenta con el numero ingresado, por favor digite un numero distinto";
        } else if (numeroCuenta < 1000000 || numeroCuenta > 9999999){
            return "El numero de cuenta debe de ser de 7 digitos";
        } else {
            if (saldo < 50000) {
                return "Debe de crear una cuenta con un saldo minimo de 50000 colones";
            } else if (existeCliente(idCliente)){
                for (int i = 0; i < clientes.size(); i++){
                    Cliente c = clientes.get(i);
                    if (c.getIdentificacion() == idCliente) {
                        Corriente tmpCuenta = new Corriente(numeroCuenta, saldo, fechaApertura, c);
                        cuentas.add(tmpCuenta); // Agrego una nueva cuanta al array list de cuentas
                        return "Cuenta creada con exito, info registrada: " + tmpCuenta;
                    }
                }
            }
        }
        return "No existe un cliente con ese numero de identificacion";
    }

    public static boolean existeCuentaCorriente (int numCuenta) {
        for (int i = 0; i < cuentas.size(); i++){
            Corriente c = cuentas.get(i);
            if (c.getNumeroCuenta() == numCuenta) {
                return true;
            }
        }
        return false;
    }

    public static String realizarDepositoCorriente (int numCuenta, double montoDeposito) {
        LocalDateTime dateMovimiento = LocalDateTime.now();

        if (existeCuentaCorriente(numCuenta)){
            for (int i = 0; i < cuentas.size(); i++){
                Corriente c = cuentas.get(i);
                if (c.getNumeroCuenta() == numCuenta) {
                    if (montoDeposito == 0 || montoDeposito < 0){
                        return "No puede hacer depositos de 0 o numeros negativos";
                    } else
                        c.setSaldo(c.getSaldo() + montoDeposito);
                }
            }
            return "Deposito realizado con exito" + "\n--Datos del movimiento--" + "\nDescripcion: Deposito" + "\nFecha y hora del movimiento: " + dateMovimiento + "\nMonto del movimiento: " + montoDeposito;
        } else {
            return "No existe una cuenta con el numero ingresado, por favor intente denuevo digitando un numero de cuenta existente";
        }
    }


    public static String realizarRetiroCorriente (int numCuenta, double montoRetiro) {
        LocalDateTime dateMovimiento = LocalDateTime.now();

        if (existeCuentaCorriente(numCuenta)){
            for (int i = 0; i < cuentas.size(); i++){
                Corriente c = cuentas.get(i);
                if (c.getNumeroCuenta() == numCuenta) {
                    if (montoRetiro == 0 || montoRetiro < 0){
                        return "No puede hacer depositos de 0 o numeros negativos";
                    } else if (montoRetiro > c.getSaldo()){
                        return "No puede retirar un monto mayor al que ya hay en la cuenta";
                    } else
                        c.setSaldo(c.getSaldo() - montoRetiro);
                }
            }


            return "Retiro realizado con exito" + "\n--Datos del movimiento--" + "\nDescripcion: Retiro" + "\nFecha y hora del movimiento: " + dateMovimiento + "\nMonto del movimiento: " + montoRetiro;
        } else {
            return "No existe una cuenta con el numero ingresado, por favor intente denuevo digitando un numero de cuenta existente";
        }
    }

    public static String mostrarSaldoCuentaCorriente (int numCuenta) {
        for (int i = 0; i < cuentas.size(); i++){
            Corriente c = cuentas.get(i);
            if (c.getNumeroCuenta() == numCuenta) {
                if (c.getSaldo() == 0){
                    return "Cuenta no tiene fondos";
                } else
                    return "El saldo de la cuenta " + numCuenta + " es de: " + c.getSaldo();
            }
        }
        return "El numero de cuenta ingresado no existe";
    }

    public static String registrarCuentasAhorro (int numeroCuenta, Double saldo, LocalDate fechaApertura, long idCliente) {
        if (existeCuentaAhorro(numeroCuenta)) {
            return "Ya existe una cuenta con el numero ingresado, por favor digite un numero distinto";
        } else if (numeroCuenta < 1000000 || numeroCuenta > 9999999){
            return "El numero de cuenta debe de ser de 7 digitos";
        } else {
            if (saldo < 50000) {
                return "Debe de crear una cuenta con un saldo minimo de 50000 colones";
            } else if (existeCliente(idCliente)){
                for (int i = 0; i < clientes.size(); i++){
                    Cliente c = clientes.get(i);
                    if (c.getIdentificacion() == idCliente) {
                        CuentaAhorro tmpCuenta = new CuentaAhorro(numeroCuenta, saldo, fechaApertura, c);
                        cuentasAhorro.add(tmpCuenta); // Agrego una nueva cuanta al array list de cuentas de ahorro
                        return "Cuenta creada con exito, info registrada: " + tmpCuenta;
                    }
                }
            }
        }
        return "No existe un cliente con ese numero de identificacion";
    }

    public static String realizarDepositoAhorro (int numCuenta, double montoDeposito) {
        double rebajaInteres;
        LocalDateTime dateMovimiento = LocalDateTime.now();

        if (existeCuentaAhorro(numCuenta)){
            for (int i = 0; i < cuentasAhorro.size(); i++){
                CuentaAhorro c = cuentasAhorro.get(i);
                if (c.getNumeroCuenta() == numCuenta) {
                    if (montoDeposito == 0 || montoDeposito < 0){
                        return "No puede hacer depositos de 0 o numeros negativos";
                    } else
                        rebajaInteres = montoDeposito * CuentaAhorro.getTasaInteres();
                        c.setSaldo(c.getSaldo() + (montoDeposito - rebajaInteres));
                }
            }

            return "Deposito realizado con exito" + "\n--Datos del movimiento--" + "\nDescripcion: Deposito" + "\nFecha y hora del movimiento: " + dateMovimiento + "\nMonto del movimiento: " + montoDeposito;
        } else {
            return "No existe una cuenta con el numero ingresado, por favor intente denuevo digitando un numero de cuenta existente";
        }
    }

    public static String realizarRetiroAhorro (int numCuenta, double montoRetiro) {
        LocalDateTime dateMovimiento = LocalDateTime.now();

        if (existeCuentaAhorro(numCuenta)){
            for (int i = 0; i < cuentasAhorro.size(); i++){
                CuentaAhorro c = cuentasAhorro.get(i);
                if (c.getNumeroCuenta() == numCuenta) {
                    if (montoRetiro <= 0  ){
                        return "No puede hacer depositos de 0 o numeros negativos";
                    } else if (montoRetiro > c.getSaldo()){
                        return "No puede retirar un monto mayor al que ya hay en la cuenta";
                    } else if (c.getSaldo() <= 100000){
                        return "No puede hacer retiros si el saldo de la cuenta es menor a los 100mil Colones";
                    } else if (montoRetiro > (c.getSaldo() * 0.50)){
                        return "El monto del retiro no puede ser mayor al 50% del saldo dentro de la cuenta";
                    } else {
                        c.setSaldo(c.getSaldo() - montoRetiro);
                    }
                }
            }
            return "Retiro realizado con exito" + "\n--Datos del movimiento--" + "\nDescripcion: Retiro" + "\nFecha y hora del movimiento: " + dateMovimiento + "\nMonto del movimiento: " + montoRetiro;
        } else {
            return "No existe una cuenta con el numero ingresado, por favor intente denuevo digitando un numero de cuenta existente";
        }
    }

    public static boolean existeCuentaAhorro (int numCuenta) {
        for (int i = 0; i < cuentasAhorro.size(); i++){
            CuentaAhorro c = cuentasAhorro.get(i);
            if (c.getNumeroCuenta() == numCuenta) {
                return true;
            }
        }
        return false;
    }

    public static String mostrarSaldoCuentaAhorro (int numCuenta) {
        for (int i = 0; i < cuentasAhorro.size(); i++){
            CuentaAhorro c = cuentasAhorro.get(i);
            if (c.getNumeroCuenta() == numCuenta) {
                if (c.getSaldo() == 0){
                    return "Cuenta no tiene fondos";
                } else
                    return "El saldo de la cuenta " + numCuenta + " es de: " + c.getSaldo();
            }
        }
        return "El numero de cuenta ingresado no existe";
    }

    public static String registrarCuentasAhorroProgramado(int numeroCuenta, Double saldo, LocalDate fechaApertura, int numAVincular, double mtDebitar){
        if (existeCuentaAhorroP(numeroCuenta)) {
            return "Ya existe una cuenta con el numero ingresado, por favor digite un numero distinto";
        } else if (numeroCuenta < 1000000 || numeroCuenta > 9999999){
            return "El numero de cuenta debe de ser de 7 digitos";
        } else {
                if (existeCuentaCorriente(numAVincular)){
                for (int i = 0; i < cuentas.size(); i++){
                    Corriente c = cuentas.get(i);
                    if (c.getNumeroCuenta() == numAVincular) {
                        Cliente cl = c.getPropietario();
                        CuentaAhorroProg tmpCuenta = new CuentaAhorroProg(numeroCuenta, saldo, fechaApertura, cl, c, mtDebitar);
                        cuentasAhorroP.add(tmpCuenta); // Agrego una nueva cuanta al array list de cuentas de ahorro programado
                        return "Cuenta creada con exito, info registrada: " + tmpCuenta;
                    }
                }
            }
        }
        return "No existe una cuenta con ese numero de cuenta";
    }

    public static String realizarDepositoAhorroProg(int numCuenta){
        LocalDate today = LocalDate.now();
        double rebajaInteres;

        if (existeCuentaAhorroP(numCuenta)){
            for (int i = 0; i < cuentasAhorroP.size(); i++){
                CuentaAhorroProg c = cuentasAhorroP.get(i);
                int tiempoDesdeAprt = Period.between(c.getUltimoDeposito(), today).getMonths();
                if (tiempoDesdeAprt >= 1){
                    if (c.getNumeroCuenta() == numCuenta){
                        if (c.getCuentaVinculada().getSaldo() > c.getMontoADebitar()){
                            c.getCuentaVinculada().setSaldo(c.getCuentaVinculada().getSaldo() - c.getMontoADebitar());
                            rebajaInteres = c.getMontoADebitar() * CuentaAhorroProg.getTasaInteres();
                            c.setSaldo(c.getSaldo() + (c.getMontoADebitar() - rebajaInteres));
                            c.setUltimoDeposito(LocalDate.now());
                        } else {
                            return "El saldo en la cuenta es menor al que quiere debitar, por favor intente nuevamente cuando hayan fondos suficientes en su cuenta corriente";
                        }
                    }
                } else {
                    return "Solo se puede depositar una vez al mes, por favor espere a que haya pasado un mes desde el ultimo deposito";
                }
            }
            return "Deposito realizado con exito";
        } else {
            return "No existe una cuenta con el numero ingresado, por favor intente denuevo digitando un numero de cuenta existente";
        }
    }

    public static String realizarRetiroAhorroProg(int numCuenta, double montoRetiro){
        LocalDate dateMovimiento = LocalDate.now();

        if (existeCuentaAhorroP(numCuenta)){
            for (int i = 0; i < cuentasAhorroP.size(); i++){
                CuentaAhorroProg c = cuentasAhorroP.get(i);
                int tiempoDesdeAprt = Period.between(c.getFechaApertura(), dateMovimiento).getYears();
                if (c.getNumeroCuenta() == numCuenta) {
                    if (montoRetiro <= 0  ){
                        return "No puede hacer depositos de 0 o numeros negativos";
                    } else if (montoRetiro > c.getSaldo()){
                        return "No puede retirar un monto mayor al que ya hay en la cuenta";
                    } else if (tiempoDesdeAprt >= 1){
                        c.setSaldo(c.getSaldo() - montoRetiro);
                    } else {
                        return "No puede retirar fondos de esta cuenta hasta que haya pasado un minimo de un annio desde que se creo la cuenta";
                    }
                }
            }
            return "Retiro realizado con exito" + "\n--Datos del movimiento--" + "\nDescripcion: Retiro" + "\nFecha y hora del movimiento: " + dateMovimiento + "\nMonto del movimiento: " + montoRetiro;
        } else {
            return "No existe una cuenta con el numero ingresado, por favor intente denuevo digitando un numero de cuenta existente";
        }
    }

    public static boolean existeCuentaAhorroP (int numCuenta) {
        for (int i = 0; i < cuentasAhorroP.size(); i++){
            CuentaAhorroProg c = cuentasAhorroP.get(i);
            if (c.getNumeroCuenta() == numCuenta) {
                return true;
            }
        }
        return false;
    }

}
