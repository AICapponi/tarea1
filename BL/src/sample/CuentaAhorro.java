package sample;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase hija de Cuenta, para instanciar las cuentas de Ahorro
 */
public class CuentaAhorro extends Cuenta{

     static private final double tasaInteres = 0.05;

    /**
     * Constructor por defecto de la clase
     */
    public CuentaAhorro(){

    }

    /**
     * Constructor para asignarle los valores a los atributos de la clase
     * @param numeroCuenta Numero de cuenta de la cuenta abierta por el cliente
     * @param saldo Saldo que hay en la cuenta
     * @param fechaApertura Fecha en la que la cuenta ha sido creada
     * @param propietario Propietario de la cuenta que se esta abriendo
     */
    public CuentaAhorro(int numeroCuenta, double saldo, LocalDate fechaApertura, Cliente propietario) {
        super(numeroCuenta, saldo, fechaApertura, propietario);
    }

    /**
     * Get de la tasa de interes fija
     * @return Valor de la tasa de interes
     */
    public static double getTasaInteres() { return tasaInteres; }

    /**
     * Metodo toString para devolver el string de los valores de los atributos de la clase
     * @return valores de los atributos de la clase
     */
    @Override
    public String toString() {
        return "Cuenta{" +
                "numeroCuenta=" + getNumeroCuenta() +
                ", saldo=" + getSaldo() +
                ", fechaApertura='" + getFechaApertura() + '\'' +
                ", propietario=" + getPropietario() +
                '}';
    }

}
