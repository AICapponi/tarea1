package sample;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase hija de su padre Cuenta, para crear instancias de la misma.
 */
public class Corriente extends Cuenta{


    /**
     * Metodo constructor por defecto
     */
    public Corriente(){

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     */
    public Corriente(int numeroCuenta, double saldo, LocalDate fechaApertura, Cliente propietario) {
        super(numeroCuenta, saldo, fechaApertura, propietario);
    }

    /**
     * toString para la impresion de los datos
     * @return
     */
    @Override
    public String toString() {
        return "Cuenta{" +
                "numeroCuenta=" + getNumeroCuenta() +
                ", saldo=" + getSaldo() +
                ", fechaApertura='" + getFechaApertura() + '\'' +
                ", propietario=" + getPropietario() +
                '}';
    }
}
